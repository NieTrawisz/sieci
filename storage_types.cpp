// Paweł Kolendo, 302860
// Michał Kasperek, 302857
// Michał Gorczyca, 302846

#include "storage_types.hpp"

std::size_t PackageQueue::size() const{
    return stockpile.size();
}

bool PackageQueue::empty() const{
    return stockpile.size()==0;
}

void PackageQueue::push(Package&& p) {
    stockpile.push_back(std::move(p));
}

IPackageQueue* PackageQueue::get_queue(){
    return dynamic_cast<IPackageQueue*>(this);;
}


IPackageStockpile::const_iterator PackageQueue::cbegin() const{return stockpile.cbegin();}

IPackageStockpile::const_iterator PackageQueue::cend() const{ return  stockpile.cend();}

IPackageStockpile::const_iterator PackageQueue::begin() const{return stockpile.cbegin();}

IPackageStockpile::const_iterator PackageQueue::end() const{ return  stockpile.cend();}

PackageQueue::PackageQueue(PackageQueueType p_type) {p_=p_type;}

Package PackageQueue::pop() {
    switch( p_ )
    {
        case PackageQueueType ::LIFO:{
            Package p=std::move(stockpile.back());
            stockpile.pop_back();
            return p;}
        case PackageQueueType ::FIFO:{
            Package pp=std::move(stockpile.front());
            stockpile.pop_front();
            return pp;}
        default:
            return Package();
    }

}
PackageQueueType PackageQueue::get_queue_type() const {return p_;}