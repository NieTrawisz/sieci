// Paweł Kolendo, 302860
// Michał Kasperek, 302857
// Michał Gorczyca, 302846

#ifndef SIECI_REPORTS_HPP
#define SIECI_REPORTS_HPP

#include "factory.hpp"

class SpecificTurnsReportNotifier{

public:

    SpecificTurnsReportNotifier(std::set<Time> turns) : turns_(std::move(turns)) {}

    bool should_generate_report(Time t);

private:

    std::set<Time> turns_;

};

class IntervalReportNotifier{

public:

    IntervalReportNotifier(TimeOffset to) : to_(to) {}

    bool should_generate_report(Time t);

private:

    TimeOffset to_;
};

#endif //SIECI_REPORTS_HPP
