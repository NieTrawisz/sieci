// Paweł Kolendo, 302860
// Michał Kasperek, 302857
// Michał Gorczyca, 302846

#include "reports.hpp"

bool SpecificTurnsReportNotifier::should_generate_report(Time t){
    for(auto e : turns_){
        if(e==t){
            return true;
        }
    }
    return false;
}

bool IntervalReportNotifier::should_generate_report(Time t){

    if(t%to_==1){
        return true;
    }
    return false;
}
