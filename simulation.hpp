// Paweł Kolendo, 302860
// Michał Kasperek, 302857
// Michał Gorczyca, 302846

#ifndef SIECI_SIMULATION_HPP
#define SIECI_SIMULATION_HPP

#include "factory.hpp"

void simulate(Factory& f, TimeOffset d, std::function<void(Factory&, Time)>);


#endif //SIECI_SIMULATION_HPP
