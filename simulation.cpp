// Paweł Kolendo, 302860
// Michał Kasperek, 302857
// Michał Gorczyca, 302846

#include "simulation.hpp"

void simulate(Factory& f, TimeOffset d, std::function<void(Factory&, Time)> fun){
    for(int i=0;i<d;i++){
        f.do_deliveries(i);
        f.do_package_passing();
        fun(f,i);
    }
}