// Paweł Kolendo, 302860
// Michał Kasperek, 302857
// Michał Gorczyca, 302846
#include "factory.hpp"
#include "helpers.hpp"
#include <algorithm>
/*template <typename Node>
void NodeCollection<Node>::add(Node& node){
    nodes.push_back(std::move(node));
}
template <typename Node>
void NodeCollection<Node>::remove_by_id(ElementID id) {
    auto it=find_by_id(id);
    if(it!=nodes.end())nodes.erase(it);
}
template <typename Node>
typename NodeCollection<Node>::iterator NodeCollection<Node>::find_by_id(ElementID id) {
        for (auto el=begin();el!=end();el++){if(el->get_id()==id)return el;}
        return nodes.end();
    }
template <typename Node>
typename NodeCollection<Node>::const_iterator NodeCollection<Node>::find_by_id(ElementID id) const {
    for (auto el=cbegin();el!=cend();el++){if(el->get_id()==id)return el;}
    return nodes.cend();
}
template <typename Node>
typename NodeCollection<Node>::const_iterator NodeCollection<Node>::cbegin() const { return nodes.cbegin();}
template <typename Node>
typename NodeCollection<Node>::const_iterator NodeCollection<Node>::cend() const { return nodes.cend();}
template <typename Node>
typename NodeCollection<Node>::iterator NodeCollection<Node>::begin() { return nodes.begin();}
template <typename Node>
typename NodeCollection<Node>::iterator NodeCollection<Node>::end() { return nodes.end();}
template <typename Node>
const Node&  NodeCollection<Node>::operator[](std::size_t index) const { return nodes[index];}
template <typename Node>
Node&  NodeCollection<Node>::operator[](std::size_t index) { return nodes[index];}
template <typename Node>
void Factory::remove_receiver(NodeCollection<Node>& collection,ElementID Id){

   for(auto& el:collection){

        for(const auto& rec:el.receiver_preferences_){

            if(rec.get_id()==Id){el.remove(rec);break;}

        }

    }

}*/

void Factory::add_ramp(Ramp &&p){
    nodes_ramp_.add(p);
}

void Factory::remove_ramp(ElementID id){
    nodes_ramp_.remove_by_id(id);
}
NodeCollection<Ramp>::iterator Factory::find_ramp_by_id(ElementID id){
    return nodes_ramp_.find_by_id(id);
}

NodeCollection<Ramp>::const_iterator Factory::find_ramp_by_id(ElementID id) const {
    return nodes_ramp_.find_by_id(id);
}

void Factory::add_worker(Worker &&p){
    nodes_worker_.add(p);
}

void Factory::remove_worker(ElementID id) {

    remove_receiver(nodes_ramp_,id);
    remove_receiver(nodes_worker_,id);
    nodes_worker_.remove_by_id(id);
}

NodeCollection<Worker>::iterator Factory::find_worker_by_id(ElementID id){
    return nodes_worker_.find_by_id(id);
}

NodeCollection<Worker>::const_iterator Factory::find_worker_by_id(ElementID id) const {
    return nodes_worker_.find_by_id(id);
}

void Factory::add_storehouse(Storehouse &&p) {
    nodes_storehouse_.add(p);
}

void Factory::remove_storehouse(ElementID id){
    remove_receiver(nodes_ramp_,id);
    remove_receiver(nodes_worker_,id);
    nodes_storehouse_.remove_by_id(id);
    std::map<const PackageSender*,int> a={};
    a[&(*nodes_ramp_.begin())]=1;
}

NodeCollection<Storehouse>::const_iterator Factory::find_storehouse_by_id(ElementID id) const {
    return nodes_storehouse_.find_by_id(id);
}

NodeCollection<Storehouse>::iterator Factory::find_storehouse_by_id(ElementID id){
    return nodes_storehouse_.find_by_id(id);
}
void Factory::do_work(int Time) {
    for(auto& n : nodes_worker_){
        n.do_work(Time);
    }
}

void Factory::do_deliveries(int Time) {
    for(auto& n : nodes_ramp_){
        n.deliver_goods(Time);
    }
}

void Factory::do_package_passing() {
    for(auto& n : nodes_ramp_){
        n.send_package();
    }
    for(auto& n : nodes_worker_){
        n.send_package();
    }
}
bool has_reachable_storehouse(const PackageSender* sender, std::map<const PackageSender*, NodeColor>& node_colors){
    if(node_colors[sender]==VERIFIED){return true;}
    node_colors[sender]=VISITED;
    if(sender->receiver_preferences_.get_preferences().empty()){
        throw std::logic_error( "logic error" );
    }
    bool has_reachable=false;

    for(const auto& el:sender->receiver_preferences_){
        if(el.first->get_receiver_type()==STOREHOUSE){
            has_reachable=true;
        }
        else if(el.first->get_receiver_type()==WORKER){

            IPackageReceiver* receiver_ptr = el.first;
            auto worker_ptr = dynamic_cast<Worker*>(receiver_ptr);
            auto sendrecv_ptr = dynamic_cast<PackageSender*>(worker_ptr);
            if(dynamic_cast<PackageSender*>(worker_ptr)==sender)continue;
            has_reachable=true;
            if(node_colors[sendrecv_ptr]==UNVISITED)has_reachable_storehouse(sendrecv_ptr,node_colors);
        }
    }
    node_colors[sender]=VERIFIED;
    if(has_reachable)return true;
    else throw std::logic_error( "logic error" );
}
bool Factory::is_consistent() {
    std::map<const PackageSender*, NodeColor> node_colors={};
    for(auto& n:  nodes_ramp_){
        node_colors[&n]=UNVISITED;
    }
    for(auto& n: nodes_worker_){
        node_colors[&n]=UNVISITED;
    }
    try{
        for(auto& n:  nodes_ramp_){
            has_reachable_storehouse(&n,node_colors);
        }
        return true;
    }catch (std::logic_error& p){
        return false;
    }


}
NodeCollection<Ramp>::const_iterator Factory::ramp_cbegin() const{ return nodes_ramp_.cbegin();}
NodeCollection<Ramp>::const_iterator Factory::ramp_cend() const { return nodes_ramp_.cend();}
NodeCollection<Worker>::const_iterator Factory::worker_cbegin() const { return nodes_worker_.cbegin();}
NodeCollection<Worker>::const_iterator Factory::worker_cend() const { return nodes_worker_.cend();}
NodeCollection<Storehouse>::const_iterator Factory::storehouse_cbegin() const { return nodes_storehouse_.cbegin();}
NodeCollection<Storehouse>::const_iterator Factory::storehouse_cend() const { return nodes_storehouse_.cend();}

ParsedLineData parse_line(std::string line){
    std::vector<std::string> tokens;
    std::string token;
    ParsedLineData p = ParsedLineData();

    std::istringstream token_stream(line);
    char delimiter = ' ';

    while (std::getline(token_stream, token, delimiter)) {
        tokens.push_back(token);
    }
    if(tokens[0]=="LOADING_RAMP"){
        p.element_type=ramp;
        p.parameters["id"]=tokens[1].substr(3,-1);
        p.parameters["delivery-interval"]=tokens[2].substr(18,-1);

    }
    else if(tokens[0]=="WORKER"){
        p.element_type=worker;
        p.parameters["id"]=tokens[1].substr(3,-1);
        p.parameters["processing-time"]=tokens[2].substr(16,-1);
        p.parameters["queue-type"]=tokens[3].substr(11,-1);
    }
    else if(tokens[0]=="STOREHOUSE"){
        p.element_type=storehouse;
        p.parameters["id"]=tokens[1].substr(3,-1);
    }
    else if(tokens[0]=="LINK"){
        p.element_type=link;
        p.parameters["src"]=tokens[1].substr(4,-1);
        p.parameters["dest"]=tokens[2].substr(5,-1);
    }
    return p;

}

Factory load_factory_structure(std::istream& is){
    Factory f;
    std::string line;
    while(std::getline(is,line,'\n')){
        if(line.empty()||*line.begin()=='/'||*line.begin()==';'||*line.begin()=='='){
            continue;
        }
        else{
            ParsedLineData product = parse_line(line);
            if(product.element_type==ramp) {
                ElementID id = std::atoi(product.parameters["id"].c_str());
                TimeOffset di = std::atoi(product.parameters["delivery-interval"].c_str());
                Ramp r(id, di);
                f.add_ramp(std::move(r));
            }
            else if(product.element_type==worker){
                ElementID id = std::atoi(product.parameters["id"].c_str());
                TimeOffset pd = std::atoi(product.parameters["processing-time"].c_str());
                PackageQueueType p_type;
                if(product.parameters["queue-type"]=="FIFO"){
                    p_type = FIFO;
                }
                else if(product.parameters["queue-type"]=="LIFO"){
                    p_type = LIFO;
                }
                std::unique_ptr<IPackageQueue> q = std::make_unique<PackageQueue>(p_type);
                Worker w(id,pd,std::move(q));
                f.add_worker(std::move(w));
            }
            else if(product.element_type==storehouse){
                ElementID id = std::atoi(product.parameters["id"].c_str());
                std::unique_ptr<IPackageStockpile> d = std::make_unique<PackageQueue>(LIFO);
                Storehouse s(id,std::move(d));
                f.add_storehouse(std::move(s));
            }
            else if(product.element_type==link){
                std::vector<std::string> first;
                std::vector<std::string> second;
                char delimiter = '-';
                first.push_back( product.parameters["src"].substr(0, product.parameters["src"].find(delimiter)));
                second.push_back( product.parameters["src"].substr(product.parameters["src"].find(delimiter)+1, product.parameters["src"].size()));
                first.push_back( product.parameters["dest"].substr(0, product.parameters["dest"].find(delimiter)));
                second.push_back( product.parameters["dest"].substr(product.parameters["dest"].find(delimiter)+1, product.parameters["src"].size()));
                if(first[0]=="ramp"&&first[1]=="worker"){
                    auto ramp = f.find_ramp_by_id(std::atoi(second[0].c_str()));
                    auto worker = f.find_worker_by_id(std::atoi(second[1].c_str()));
                    IPackageReceiver* ipr = &(*worker);
                    ramp->receiver_preferences_.add_receiver(ipr);
                }
                if(first[0]=="ramp"&&first[1]=="store"){
                    auto ramp = f.find_ramp_by_id(std::atoi(second[0].c_str()));
                    auto s = f.find_storehouse_by_id(std::atoi(second[1].c_str()));
                    IPackageReceiver* ipr = &(*s);
                    ramp->receiver_preferences_.add_receiver(ipr);
                }
                else if(first[0]=="worker"&&first[1]=="worker"){
                    auto w1 = f.find_worker_by_id(std::atoi(second[0].c_str()));
                    auto w2 = f.find_worker_by_id(std::atoi(second[1].c_str()));
                    IPackageReceiver* ipr = &(*w2);
                    w1->receiver_preferences_.add_receiver(ipr);
                }
                else if(first[0]=="worker"&&first[1]=="store"){
                    auto w = f.find_worker_by_id(std::atoi(second[0].c_str()));
                    auto s = f.find_storehouse_by_id(std::atoi(second[1].c_str()));
                    IPackageReceiver* ipr = &(*s);
                    w->receiver_preferences_.add_receiver(ipr);
                }

            }
        }
    }
    return f;
}


void save_factory_structure(Factory& factory, std::ostream& os){

    os << "; == LOADING RAMPS ==\n\n";
    for(auto it = factory.ramp_cbegin(); it != factory.ramp_cend(); it++){
        os << "LOADING_RAMP id="<<it->get_id()<<" delivery-interval="<<it->get_delivery_interval()<<"\n";
    }
    os << "\n; == WORKERS ==\n\n";
    for(auto it = factory.worker_cbegin(); it != factory.worker_cend(); it++){
        os << "WORKER id="<<it->get_id()<<" processing-time="<<it->get_processing_duration()<<" queue-type=";
        if(it->get_queue_type()==FIFO){
            os<<"FIFO\n"; 
        }
        else{
            os<<"LIFO\n";
        }
        
    }
    os << "\n; == STOREHOUSE ==\n\n";
    for(auto it = factory.storehouse_cbegin(); it != factory.storehouse_cend(); it++){
        os << "STOREHOUSE id="<<it->get_id()<<"\n";
    }
    os << "\n; == LINKS ==\n\n";
    for(auto it = factory.ramp_cbegin(); it != factory.ramp_cend(); it++) {
        for (auto e = it->receiver_preferences_.get_preferences().begin();
             e != it->receiver_preferences_.get_preferences().end(); e++) {
            os << "LINK src=ramp-" << it->get_id() << " dest=";
            if(e->first->get_receiver_type()==WORKER){
                os << "worker-"<<e->first->get_id()<<"\n";
            }
            else if(e->first->get_receiver_type()==STOREHOUSE){
                os << "store-"<<e->first->get_id()<<"\n";
            }
        }

    }
    for(auto it = factory.worker_cbegin(); it != factory.worker_cend(); it++){
        for(auto e = it->receiver_preferences_.get_preferences().begin(); e  != it->receiver_preferences_.get_preferences().end(); e++){
            os << "\nLINK src=worker-"<<it->get_id()<<" dest=";
            if(e->first->get_receiver_type()==WORKER){
                os << "worker-"<<e->first->get_id()<<"\n";
            }
            else if(e->first->get_receiver_type()==STOREHOUSE){
                os << "store-"<<e->first->get_id()<<"\n";
            }

        }
    }
}