// Paweł Kolendo, 302860
// Michał Kasperek, 302857
// Michał Gorczyca, 302846
#include "nodes.hpp"
#include "iostream"
void ReceiverPreferences::add_receiver(IPackageReceiver *r) {
    std::size_t size=receivers.size();
    if(size==0)receivers[r]=1;
    else {
        double p= 1 / (double) size;
        receivers[r] = p;
        p = 1/ (double) (size + 1);
        for (auto &el:receivers) {
            el.second = p;
        }
    }
}
void ReceiverPreferences::remove_receiver(IPackageReceiver *r) {
    receivers.erase(r);
    std::size_t size=receivers.size();
    double p = 1 / (double) size;
    for (auto &el:receivers) {
        el.second = p;
    }
}
IPackageReceiver* ReceiverPreferences::choose_receiver() const{
    double p=prob();
    double dist=0;
    for(auto& el:receivers){
        dist+=el.second;
        IPackageReceiver* chosen=(el.first);
        if(dist>p)return chosen;
    }
    return nullptr;
}

const ReceiverPreferences::preferences_t& ReceiverPreferences::get_preferences() const {
    return receivers;
}

void PackageSender::push_package(Package&& p) {
    current_package=std::move(p);
}
const PackBuf& PackageSender::get_sending_buffer() const {
    return current_package;
}
void PackageSender::send_package(){
    IPackageReceiver* receiver=receiver_preferences_.choose_receiver();
    if(receiver==nullptr){return;}
    if(current_package)receiver->receive_package(std::move(current_package.value()));
    current_package.reset();
}

PackageSender::PackageSender(std::function<double()> Generator) :receiver_preferences_(Generator){}

PackageSender::PackageSender() :receiver_preferences_(probability_generator){}

ReceiverPreferences::ReceiverPreferences(std::function<double()> Generator) {prob=Generator;}

ReceiverPreferences::ReceiverPreferences() {prob=probability_generator;}

ReceiverPreferences::const_iterator ReceiverPreferences::cbegin() const noexcept {return receivers.cbegin();}

ReceiverPreferences::const_iterator ReceiverPreferences::cend() const noexcept {return receivers.cend();}

ReceiverPreferences::const_iterator ReceiverPreferences::begin() const noexcept {return receivers.cbegin();}

ReceiverPreferences::const_iterator ReceiverPreferences::end() const noexcept {return receivers.cend();}

ReceiverType Storehouse::get_receiver_type() const {
    return ReceiverType::STOREHOUSE;
}

ReceiverType Worker::get_receiver_type() const {
    return ReceiverType::WORKER;
}

Storehouse::Storehouse(ElementID id) {id_=id; d_=std::make_unique<PackageQueue>(LIFO);}

Storehouse::Storehouse(ElementID id, std::unique_ptr<IPackageStockpile> d) {
    id_= id;
    d_ =std::move(d);
}
ElementID Storehouse::get_id() const {return id_;}
void Storehouse::receive_package(Package &&p) { d_->push(std::move(p)); }
stock::const_iterator Storehouse::cbegin() const {return d_->cbegin();}
stock::const_iterator Storehouse::begin() const {return d_->begin();}
stock::const_iterator Storehouse::cend() const {return d_->cend();}
stock::const_iterator Storehouse::end() const {return d_->end();}

stock::const_iterator Worker::cbegin() const {return q_->cbegin();}
stock::const_iterator Worker::begin() const {return q_->begin();}
stock::const_iterator Worker::cend() const {return q_->cend();}
stock::const_iterator Worker::end() const {return q_->end();}
void Ramp::deliver_goods(Time t) {

    if(t-last_delivery_==di_||t==1){
        Package p;
        push_package(std::move(p));
        last_delivery_ = t;
    }

}



Time Worker::get_package_procesing_start_time(){

    if(start_processing_){
        return start_processing_;
    }

    return 1;
}

const std::optional<Package>& Worker::get_processing_bufer() const{
    return get_sending_buffer();
}

IPackageQueue* Worker::get_queue() const{
    return q_->get_queue();
}

PackageQueueType Worker::get_queue_type() const{
    return q_->get_queue_type();
}

void Worker::do_work(Time t) {

    if(get_sending_buffer()==std::nullopt) {

        push_package(q_->pop());

        start_processing_ = t;

    }

    else if(get_sending_buffer()!= std::nullopt && t-start_processing_==pd_){

        push_package(std::move(current_package.value()));

        send_package();

    }

}