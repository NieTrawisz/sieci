// Paweł Kolendo, 302860
// Michał Kasperek, 302857
// Michał Gorczyca, 302846

#ifndef SIECI_NODES_HPP
#define SIECI_NODES_HPP
#include <map>
#include <optional>
#include "package.hpp"
#include "helpers.hpp"
#include "storage_types.hpp"
#include <memory>
enum ReceiverType{
WORKER,
STOREHOUSE
};
class IPackageReceiver{
public:
    using const_iterator=IPackageStockpile::const_iterator;
    virtual void receive_package(Package&& p) =0;
    virtual ElementID get_id() const =0;
    virtual ReceiverType get_receiver_type() const =0;
    virtual const_iterator cbegin() const=0;
    virtual const_iterator cend() const=0;
    virtual const_iterator begin() const=0;
    virtual const_iterator end() const=0;
};

using PackBuf=std::optional<Package>;

class ReceiverPreferences{
public:
    using preferences_t = std::map<IPackageReceiver*, double>;
    using const_iterator = preferences_t::const_iterator;
    const preferences_t& get_preferences() const ;
    ReceiverPreferences(ReceiverPreferences&& rp)= default;
    ReceiverPreferences& operator=(const ReceiverPreferences& ps)= default;
    ReceiverPreferences();
    ReceiverPreferences(ProbabilityGenerator Generator);
    void add_receiver(IPackageReceiver* r) ;
    void remove_receiver(IPackageReceiver* r) ;
    IPackageReceiver* choose_receiver() const;
    const_iterator cbegin() const noexcept;
    const_iterator cend() const noexcept;
    const_iterator begin() const noexcept;
    const_iterator end() const noexcept;
private:
    preferences_t receivers;
    std::function<double()> prob;
};
class PackageSender{
protected:
    PackBuf current_package=std::nullopt;
    void push_package(Package&& p);
public:
    ReceiverPreferences receiver_preferences_;
    PackageSender();
    PackageSender(PackageSender&& ps)=default;
    PackageSender& operator=(const PackageSender& ps)= default;

    PackageSender(std::function<double()> Generator);
    void send_package();
    const PackBuf& get_sending_buffer() const;
};
class Storehouse:public IPackageReceiver{
public:
    Storehouse(ElementID id);
    Storehouse(ElementID id, std::unique_ptr<IPackageStockpile> d);
    void receive_package(Package&& p) override;

    ElementID get_id() const override;

    IPackageStockpile::const_iterator cbegin() const override ;
    IPackageStockpile::const_iterator cend() const override ;
    IPackageStockpile::const_iterator begin() const override ;
    IPackageStockpile::const_iterator end() const override ;
    ReceiverType get_receiver_type() const override ;
private:
    ElementID id_;
    std::unique_ptr<IPackageStockpile> d_;
};


class Ramp : public PackageSender{

public:

    Ramp(ElementID id, TimeOffset di) : id_(id), di_(di) {}

    void deliver_goods(Time t);

    TimeOffset get_delivery_interval() const { return di_;}

    ElementID get_id() const {return id_;}


private:

    ElementID id_;

    TimeOffset di_;

    int last_delivery_;

};

class Worker : public PackageSender, public IPackageReceiver{

public:

    Worker(ElementID id, TimeOffset pd, std::unique_ptr<IPackageQueue> q) : id_(id), pd_(pd), q_(std::move(q)) {}

    void do_work(Time t) ;

    TimeOffset get_processing_duration() { return pd_;}

    Time get_package_procesing_start_time();

    ElementID get_id() const override {return id_;}

    void receive_package(Package &&p) override{ q_->push(std::move(p));}

    IPackageStockpile::const_iterator cbegin() const override ;
    IPackageStockpile::const_iterator cend() const override ;
    IPackageStockpile::const_iterator begin() const override ;
    IPackageStockpile::const_iterator end() const override ;
    ReceiverType get_receiver_type() const override ;
private:

    ElementID id_;

    TimeOffset pd_;

    std::unique_ptr<IPackageQueue> q_;

    int start_processing_;
};
#endif //SIECI_NODES_HPP
