// Paweł Kolendo, 302860
// Michał Kasperek, 302857
// Michał Gorczyca, 302846

#ifndef SIECI_FACTORY_HPP
#define SIECI_FACTORY_HPP

#include <vector>
#include <cstdlib>
#include "types.hpp"
#include "nodes.hpp"
#include <string>
#include <fstream>
#include <sstream>
#include <ostream>
#include <algorithm>

extern bool has_reachable_storehouse(const PackageSender* sender, std::map<const PackageSender*, NodeColor>& node_colors);
template <typename Node>
class NodeCollection{
public:
    using container_t =typename std::list<Node>;
    using iterator = typename container_t::iterator;
    using const_iterator = typename container_t::const_iterator;
    void add(Node& node){
        nodes.push_back(std::move(node));
    }
    void remove_by_id(ElementID id){
        auto it=find_by_id(id);
        if(it!=nodes.end())nodes.erase(it);
    }
    iterator find_by_id(ElementID id){
        for (auto el=begin();el!=end();el++){if(el->get_id()==id)return el;}
        return nodes.end();
    }
    const_iterator find_by_id(ElementID id) const{
        for (auto el=cbegin();el!=cend();el++){if(el->get_id()==id)return el;}
        return nodes.cend();
    }
    const_iterator cbegin() const{ return nodes.cbegin();}
    const_iterator cend() const{ return nodes.cend();}
    iterator begin() { return nodes.begin();}
    iterator end(){ return nodes.end();}
    //const Node& operator[] (std::size_t index) const{ return *(nodes.cbegin()+index);}
    //Node& operator[] (std::size_t index){ return *(nodes.begin()+index);}
private:
    container_t nodes={};
};
class Factory{
public:

    Factory()= default;
    void add_ramp(Ramp &&p);
    void remove_ramp(ElementID id);
    NodeCollection<Ramp>::iterator find_ramp_by_id(ElementID id);
    NodeCollection<Ramp>::const_iterator find_ramp_by_id(ElementID id) const;
    NodeCollection<Ramp>::const_iterator ramp_cbegin() const;
    NodeCollection<Ramp>::const_iterator ramp_cend() const;

    void add_worker(Worker &&p);
    void remove_worker(ElementID id);
    NodeCollection<Worker>::iterator find_worker_by_id(ElementID id);
    NodeCollection<Worker>::const_iterator find_worker_by_id(ElementID id) const;
    NodeCollection<Worker>::const_iterator worker_cbegin() const;
    NodeCollection<Worker>::const_iterator worker_cend() const;

    void add_storehouse(Storehouse &&p);
    void remove_storehouse(ElementID id);
    NodeCollection<Storehouse>::iterator find_storehouse_by_id(ElementID id);
    NodeCollection<Storehouse>::const_iterator find_storehouse_by_id(ElementID id) const;
    NodeCollection<Storehouse>::const_iterator storehouse_cbegin() const;
    NodeCollection<Storehouse>::const_iterator storehouse_cend() const;


    void do_work(int Time);

    void do_deliveries(int Time);

    void do_package_passing();

    bool is_consistent();
private:
    template <typename Node>
    void remove_receiver(NodeCollection<Node>& collection,ElementID Id){
        for(auto& el:collection){
            for(const auto& rec:el.receiver_preferences_){
                if(rec.first->get_id()==Id){el.receiver_preferences_.remove_receiver(rec.first);break;}
            }
        }
    }

    NodeCollection<Ramp> nodes_ramp_;
    NodeCollection<Worker> nodes_worker_;
    NodeCollection<Storehouse> nodes_storehouse_;
};

enum ElementType{
    ramp,
    worker,
    storehouse,
    link
};


struct ParsedLineData{
    ElementType element_type;
    std::map<std::string,std::string> parameters;
};

ParsedLineData parse_line(std::string line);

Factory load_factory_structure(std::istream& is);

void save_factory_structure(Factory& factory, std::ostream& os);


#endif //SIECI_FACTORY_HPP

