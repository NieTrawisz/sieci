// Paweł Kolendo, 302860
// Michał Kasperek, 302857
// Michał Gorczyca, 302846

#ifndef SIECI_FACTORY_HPP
#define SIECI_FACTORY_HPP

#include <vector>
#include "types.hpp"
#include "nodes.hpp"


enum NodeColor{
     UNVISITED,
     VISITED,
     VERIFIED,
};

bool has_reachable_storehouse(const PackageSender* sender, std::map<const PackageSender*, NodeColor>& node_colors);

template <typename Node>
class NodeCollection{
public:
    using container_t =typename std::vector<Node>;
    using iterator = typename container_t::iterator;
    using const_iterator = typename container_t::const_iterator;
    void add(Node& node);
    void remove_by_id(ElementID id);
    iterator find_by_id(ElementID id) ;
    const_iterator find_by_id(ElementID id) const;
    const_iterator cbegin() const;
    const_iterator cend() const;
    iterator begin();
    iterator end();
    const Node& operator[] (std::size_t index) const;
    Node& operator[] (std::size_t index);
private:
    std::vector<Node> nodes={};
};
class Factory{
public:

    Factory();
    void add_ramp(Ramp &&p);
    void remove_ramp(ElementID id);
    NodeCollection<Ramp>::iterator find_ramp_by_id(ElementID id);
    NodeCollection<Ramp>::const_iterator find_ramp_by_id(ElementID id) const;
    NodeCollection<Ramp>::const_iterator ramp_cbegin() const;
    NodeCollection<Ramp>::const_iterator ramp_cend() const;

    void add_worker(Worker &&p);
    void remove_worker(ElementID id);
    NodeCollection<Worker>::iterator find_worker_by_id(ElementID id);
    NodeCollection<Worker>::const_iterator find_worker_by_id(ElementID id) const;
    NodeCollection<Worker>::const_iterator worker_cbegin() const;
    NodeCollection<Worker>::const_iterator worker_cend() const;

    void add_storehouse(Storehouse &&p);
    void remove_storehouse(ElementID id);
    NodeCollection<Storehouse>::iterator find_storehouse_by_id(ElementID id);
    NodeCollection<Storehouse>::const_iterator find_storehouse_by_id(ElementID id) const;
    NodeCollection<Storehouse>::const_iterator storehouse_cbegin() const;
    NodeCollection<Storehouse>::const_iterator storehouse_cend() const;

    template <typename Node>
    void remove_receiver(NodeCollection<Node>& collection,ElementID Id);

    void do_work(int Time);

    void do_deliveries(int Time);

    void do_package_passing();

    bool is_consistent(void);

private:
    
    NodeCollection<Ramp> nodes_ramp_;
    NodeCollection<Worker> nodes_worker_;
    NodeCollection<Storehouse> nodes_storehouse_;
};
#endif //SIECI_FACTORY_HPP
