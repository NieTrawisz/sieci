// Paweł Kolendo, 302860
// Michał Kasperek, 302857
// Michał Gorczyca, 302846

#ifndef SIECI_STORAGE_TYPES_HPP
#define SIECI_STORAGE_TYPES_HPP

#include <cstdio>
#include <list>
#include "package.hpp"

using stock=std::list<Package>;
enum PackageQueueType{
    LIFO,
    FIFO
};

class IPackageStockpile{
public:
    using const_iterator=stock::const_iterator;
    virtual void push(Package&& p)=0;
    virtual bool empty() const=0;
    virtual std::size_t size() const=0;
    virtual const_iterator cbegin() const=0;
    virtual const_iterator cend() const=0;
    virtual const_iterator begin() const=0;
    virtual const_iterator end() const=0;
};
class IPackageQueue:public IPackageStockpile{
public:
    virtual Package pop()=0;
    virtual IPackageQueue* get_queue()=0;
    virtual PackageQueueType get_queue_type() const=0;
};
class PackageQueue:public IPackageQueue{
private:
    PackageQueueType p_;
    stock stockpile={};
public:
    PackageQueue(PackageQueueType p_type);
    Package pop() override;
    PackageQueueType get_queue_type() const override;
    void push(Package&& p) override;
    IPackageQueue* get_queue() override ;
    bool empty() const override;
    std::size_t size() const override ;
    IPackageStockpile::const_iterator cbegin() const override ;
    IPackageStockpile::const_iterator cend() const override ;
    IPackageStockpile::const_iterator begin() const override ;
    IPackageStockpile::const_iterator end() const override ;
};
#endif //SIECI_STORAGE_TYPES_HPP