// Paweł Kolendo, 302860
// Michał Kasperek, 302857
// Michał Gorczyca, 302846
#include "factory.hpp"
#include <algorithm>
template <typename Node>
void NodeCollection<Node>::add(Node& node){
    nodes.push_back(std::move(node));
}
template <typename Node>
void NodeCollection<Node>::remove_by_id(ElementID id) {
    auto it=find_by_id(id);
    if(it!=nodes.end())nodes.erase(it);
}
template <typename Node>
typename NodeCollection<Node>::iterator NodeCollection<Node>::find_by_id(ElementID id) {
    for (auto el=begin();el!=end();el++){if(el->get_id()==id)return el;}
    return nodes.end();
    //auto it = std::find_if(nodes.begin(), nodes.end(),[](const auto& elem){ return (elem.get_id()==id);});
}
template <typename Node>
typename NodeCollection<Node>::const_iterator NodeCollection<Node>::find_by_id(ElementID id) const {
    for (auto el=cbegin();el!=cend();el++){if(el->get_id()==id)return el;}
    return nodes.cend();
}
template <typename Node>
typename NodeCollection<Node>::const_iterator NodeCollection<Node>::cbegin() const { return nodes.cbegin();}
template <typename Node>
typename NodeCollection<Node>::const_iterator NodeCollection<Node>::cend() const { return nodes.cend();}
template <typename Node>
typename NodeCollection<Node>::iterator NodeCollection<Node>::begin() { return nodes.begin();}
template <typename Node>
typename NodeCollection<Node>::iterator NodeCollection<Node>::end() { return nodes.end();}
template <typename Node>
const Node&  NodeCollection<Node>::operator[](std::size_t index) const { return nodes[index];}
template <typename Node>
Node&  NodeCollection<Node>::operator[](std::size_t index) { return nodes[index];}
template <typename Node>
void Factory::remove_receiver(NodeCollection<Node>& collection,ElementID Id){
    collection.remove_by_id(Id);
}

void Factory::add_ramp(Ramp &&p){
    nodes_ramp_.add(p);
}

void Factory::remove_ramp(ElementID id){
    nodes_ramp_.remove_by_id(id);
}
NodeCollection<Ramp>::iterator Factory::find_ramp_by_id(ElementID id){
    return nodes_ramp_.find_by_id(id);
}

NodeCollection<Ramp>::const_iterator Factory::find_ramp_by_id(ElementID id) const {
    return nodes_ramp_.find_by_id(id);
}

void Factory::add_worker(Worker &&p){
    nodes_worker_.add(p);
}

void Factory::remove_worker(ElementID id) {
    nodes_worker_.remove_by_id(id);
}

NodeCollection<Worker>::iterator Factory::find_worker_by_id(ElementID id){
    return nodes_worker_.find_by_id(id);
}

NodeCollection<Worker>::const_iterator Factory::find_worker_by_id(ElementID id) const {
    return nodes_worker_.find_by_id(id);
}

void Factory::add_storehouse(Storehouse &&p) {
    nodes_storehouse_.add(p);
}

void Factory::remove_storehouse(ElementID id){
    nodes_storehouse_.remove_by_id(id);
}

NodeCollection<Storehouse>::const_iterator Factory::find_storehouse_by_id(ElementID id) const {
    return nodes_storehouse_.find_by_id(id);
}

NodeCollection<Storehouse>::iterator Factory::find_storehouse_by_id(ElementID id){
    return nodes_storehouse_.find_by_id(id);
}

void Factory::do_work(int Time) {
for(auto& n : nodes_worker_){
     n.do_work(Time);
}
}

void Factory::do_deliveries(int Time) {
    for(auto& n : nodes_ramp_){
        n.deliver_goods(Time);
    }
}

void Factory::do_package_passing() {
    for(auto& n : nodes_ramp_){
        n.send_package();
    }
    for(auto& n : nodes_worker_){
        n.send_package();
    }
}

bool Factory::is_consistent() {
    std::map<const PackageSender*, NodeColor> node_colors={};
    for(auto& n:  nodes_ramp_){
        node_colors[&n]=UNVISITED;
    }
    for(auto& n: nodes_worker_){
        node_colors[&n]=UNVISITED;
    }

    bool has_reachable_storehouse(const PackageSender* sender, std::map<const PackageSender*, NodeColor>& node_colors){
        if(node_colors[sender]==VERIFIED){return true;}
        node_colors[sender]=VISITED;
        if(<>.receiver_preferences_.size()==0){
            raise logic_error( "logic error" );
        }
        has_reachable=false;

        for(const auto& el:receiverpref){
            if(el==STOREHOUSE){
                has_reachable=true;
            }
            else if(el==WORKER){
                IPackageReceiver* receiver_ptr= sender;
                auto worker_ptr = dynamic_cast<Worker*>(receiver_ptr);
                auto sendrecv_ptr = dynamic_cast<PackageSender*>(worker_ptr);
            }
        }
    }

}

NodeCollection<Ramp>::const_iterator Factory::ramp_cbegin() const{ return nodes_ramp_.cbegin();}
NodeCollection<Ramp>::const_iterator Factory::ramp_cend() const { return nodes_ramp_.cend();}
NodeCollection<Worker>::const_iterator Factory::worker_cbegin() const { return nodes_worker_.cbegin();}
NodeCollection<Worker>::const_iterator Factory::worker_cend() const { return nodes_worker_.cend();}
NodeCollection<Storehouse>::const_iterator Factory::storehouse_cbegin() const { return nodes_storehouse_.cbegin();}
NodeCollection<Storehouse>::const_iterator Factory::storehouse_cend() const { return nodes_storehouse_.cend();}